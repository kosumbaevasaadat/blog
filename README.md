# Blog REST API

## Description

This application is a REST API for a blog in which any authorized users can make entries.

## Technology stack

* Nest.js
* PostgreSQL
* TypeORM
* Passport JS
* Yandex S3

## Documentation
* [API documentation](https://documenter.getpostman.com/view/18726292/2s93eVYEF1)
