import { MigrationInterface, QueryRunner } from 'typeorm';
import { config } from 'dotenv';
import * as bcrypt from 'bcrypt';

import { User } from '../../src/entities/user.entity';
import { File } from '../../src/entities/file.entity';
import { Roles } from '../../src/shared/enums/user-roles';
import { Content } from '../../src/entities/content.entity';
import { Post } from '../../src/entities/post.entity';

export class SeedDb1683199333230 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    config();

    const encodedPassword = (await bcrypt.hash(process.env.USER_PASSWORD, 10)) as string;

    const user = await queryRunner.manager.save(
      queryRunner.manager.create<User>(User, {
        firstName: process.env.USER_FIRST_NAME,
        lastName: process.env.USER_LAST_NAME,
        email: process.env.USER_EMAIL,
        password: encodedPassword,
        role: Roles.USER,
      }),
    );

    const [image1, image2, image3, image4, image5] = await queryRunner.manager.save(
      queryRunner.manager.create<File>(File, [
        {
          name: '1683203425607.jpeg',
          fileKey: '1683203425607.jpeg',
          contentSize: 14170,
          contentType: 'image/jpeg',
        }, {
          name: '1683203435359.jpeg',
          fileKey: '1683203435359.jpeg',
          contentSize: 11751,
          contentType: 'image/jpeg',
        }, {
          name: '1683203444312.jpeg',
          fileKey: '1683203444312.jpeg',
          contentSize: 11423,
          contentType: 'image/jpeg',
        }, {
          name: '1683203451494.jpeg',
          fileKey: '1683203451494.jpeg',
          contentSize: 15632,
          contentType: 'image/jpeg',
        }, {
          name: '1683203459004.jpeg',
          fileKey: '1683203459004.jpeg',
          contentSize: 6885,
          contentType: 'image/jpeg',
        },
      ]),
    );

    const [content1, content2, content3, content4, content5] = await queryRunner.manager.save(
      queryRunner.manager.create<Content>(Content, [
        {
          description: 'Description 1',
          image: image1,
        }, {
          description: 'Description 2',
          image: image2,
        }, {
          description: 'Description 3',
          image: image3,
        }, {
          description: 'Description 4',
          image: image4,
        }, {
          description: 'Description 5',
          image: image5,
        },
      ]),
    );

    await queryRunner.manager.save(
      queryRunner.manager.create<Post>(Post, [
        {
          content: content1,
          author: user,
        }, {
          content: content2,
          author: user,
        }, {
          content: content3,
          author: user,
        }, {
          content: content4,
          author: user,
        }, {
          content: content5,
          author: user,
        },
      ]),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    config();

    await queryRunner.query(`DELETE FROM user WHERE email = ${process.env.ADMIN_EMAIL}`);
  }

}
