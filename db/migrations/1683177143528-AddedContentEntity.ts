import { MigrationInterface, QueryRunner } from "typeorm";

export class AddedContentEntity1683177143528 implements MigrationInterface {
    name = 'AddedContentEntity1683177143528'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "content" ("id" SERIAL NOT NULL, "status" character varying NOT NULL DEFAULT 'ACTIVE', "create_date" TIMESTAMP NOT NULL DEFAULT now(), "update_date" TIMESTAMP NOT NULL DEFAULT now(), "description" character varying, "image_id" integer NOT NULL, CONSTRAINT "REL_483189e118926946130d4957b9" UNIQUE ("image_id"), CONSTRAINT "PK_6a2083913f3647b44f205204e36" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "content" ADD CONSTRAINT "FK_483189e118926946130d4957b9a" FOREIGN KEY ("image_id") REFERENCES "file"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "content" DROP CONSTRAINT "FK_483189e118926946130d4957b9a"`);
        await queryRunner.query(`DROP TABLE "content"`);
    }

}
