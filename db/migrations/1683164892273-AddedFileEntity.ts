import { MigrationInterface, QueryRunner } from "typeorm";

export class AddedFileEntity1683164892273 implements MigrationInterface {
    name = 'AddedFileEntity1683164892273'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "file" ("id" SERIAL NOT NULL, "status" character varying NOT NULL DEFAULT 'ACTIVE', "create_date" TIMESTAMP NOT NULL DEFAULT now(), "update_date" TIMESTAMP NOT NULL DEFAULT now(), "name" character varying NOT NULL, "file_key" character varying NOT NULL, "content_size" integer NOT NULL, "content_type" character varying NOT NULL, CONSTRAINT "PK_36b46d232307066b3a2c9ea3a1d" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "file"`);
    }

}
