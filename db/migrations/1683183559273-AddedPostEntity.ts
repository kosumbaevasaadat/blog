import { MigrationInterface, QueryRunner } from "typeorm";

export class AddedPostEntity1683183559273 implements MigrationInterface {
    name = 'AddedPostEntity1683183559273'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "post" ("id" SERIAL NOT NULL, "status" character varying NOT NULL DEFAULT 'ACTIVE', "create_date" TIMESTAMP NOT NULL DEFAULT now(), "update_date" TIMESTAMP NOT NULL DEFAULT now(), "content_id" integer NOT NULL, "author_id" integer NOT NULL, CONSTRAINT "REL_de3a5028e4d16abe413abf49ee" UNIQUE ("content_id"), CONSTRAINT "PK_be5fda3aac270b134ff9c21cdee" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "post" ADD CONSTRAINT "FK_de3a5028e4d16abe413abf49ee5" FOREIGN KEY ("content_id") REFERENCES "content"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "post" ADD CONSTRAINT "FK_2f1a9ca8908fc8168bc18437f62" FOREIGN KEY ("author_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "post" DROP CONSTRAINT "FK_2f1a9ca8908fc8168bc18437f62"`);
        await queryRunner.query(`ALTER TABLE "post" DROP CONSTRAINT "FK_de3a5028e4d16abe413abf49ee5"`);
        await queryRunner.query(`DROP TABLE "post"`);
    }

}
