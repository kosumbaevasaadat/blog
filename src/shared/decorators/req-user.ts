import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserInterface } from '../../modules/user/intetrfaces/user.interface';

export const ReqUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): UserInterface => {
    const {
      user: { user },
    } = ctx.switchToHttp().getRequest();
    return user;
  },
);
