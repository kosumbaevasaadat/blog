export const ERRORS = {
  USER: {
    alreadyExist: 'User already exist',
    notExist: 'User does not exist',
    emailExist: 'User with such email already exist!',
  },
  AUTH: {
    password:
      'The password must contain from 8 to 64 characters, must include 2 letters (at least 1 uppercase and one lowercase), numbers and special characters!',
    noAccessToken: 'No access token!',
    wrongCredentials: 'Wrong credentials provided',
    noRefreshToken: 'No refresh token',
    invalidRefreshToken: 'Refresh token is not valid',
    accountDeleted: 'Your account was deleted',
    forbidden: 'Forbidden resource!',
  },
  GENERAL: {
    notFound: 'Not found by id = ',
    cannotAccess: 'Cannot access to the data',
  },
  FILE: {
    fileExt: 'Incorrect file extension',
  },
};

export const MESSAGES = {
  AUTH: {
    logoutSuccess: 'You have been successfully logged out!',
  },
  FILE: {
    deleted: 'File successfully deleted!',
  },
  GENERAL: {
    deleted: 'Deleted successfully!',
  },
};

export const USER = {
  id: 'id',
  email: 'email',
};

export const REGEX = {
  password: /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*?&]).{8,64}$/,
};

export const EXT = ['jpeg', 'jpg', 'png', 'gif', 'svg', 'webp', 'avif'];

export const PAGINATION = {
  page: 1,
  take: 20,
};
