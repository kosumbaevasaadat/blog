import { REGEX } from '../constants';

export const isPassword = (password: string): boolean =>
  REGEX.password.test(password);
