export interface IPaginationParams {
  take?: number;
  page?: number;
}
