export enum EntityStatus {
  ACTIVE = 'ACTIVE',
  DELETED = 'DELETED',
}
