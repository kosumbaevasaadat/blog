import { Controller, Get, UseGuards } from '@nestjs/common';

import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { User } from '../../entities/user.entity';
import { ReqUser } from '../../shared/decorators/req-user';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('me')
  @UseGuards(JwtAuthGuard)
  getProfile(@ReqUser() user: User) {
    return this.userService.getOne(user);
  }
}
