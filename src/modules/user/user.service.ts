import { Repository } from 'typeorm';
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { User } from '../../entities/user.entity';
import { BaseService } from '../../shared/lib/base.service';
import { SignUpDto } from '../auth/dto/sign-up.dto';
import { ERRORS, USER } from '../../shared/constants';

@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {
    super(userRepository, 'user');
  }

  async getOne(user: User) {
    const userData = await this.getBy({ key: 'id', value: user.id });
    return this.removePasswordOnRes(userData);
  }

  async create(user: SignUpDto) {
    const existedUser = await this.getBy({
      key: USER.email,
      value: user.email,
    });

    if (existedUser) {
      throw new BadRequestException([ERRORS.USER.alreadyExist]);
    }

    const userData = this.userRepository.create(user);
    await this.userRepository.save(userData);
    return this.removePasswordOnRes(userData);
  }

  removePasswordOnRes(user: User) {
    delete user.password;
    return user;
  }
}
