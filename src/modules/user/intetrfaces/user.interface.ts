import { Roles } from '../../../shared/enums/user-roles';
import { BaseInterface } from '../../../shared/interfaces/base.interface';

export interface UserInterface extends BaseInterface {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: Roles;
}
