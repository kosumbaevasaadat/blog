import { Request, Response } from 'express';
import * as bcrypt from 'bcrypt';
import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { UserService } from '../user/user.service';
import { IDecodedJwt } from './interfaces/decoded-jwt.interface';
import { TokensDto } from './dto/tokens.dto';
import { JWT_ACCESS_EXP, JWT_REFRESH_EXP, TOKEN_TYPES } from './auth.constants';
import { ERRORS, MESSAGES, USER } from 'src/shared/constants';
import { SignUpDto } from './dto/sign-up.dto';
import { User } from 'src/entities/user.entity';
import { EntityStatus } from 'src/shared/enums/entity-status';
import { AuthDto } from './dto/auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UserService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async signUp(
    res: Response,
    { email, password, firstName, lastName }: SignUpDto,
  ) {
    const encodedPassword = await this.encodePassword(password);
    const user = await this.usersService.create({
      firstName,
      lastName,
      email,
      ...encodedPassword,
    });
    delete user.password;

    const tokens = this.createTokens(user);

    return { user, accessToken: this.setToken(res, tokens) };
  }

  async login(res: Response, loginDto: AuthDto) {
    const user = await this.validateUser(loginDto);
    const tokens = this.createTokens(user);

    return { user, accessToken: this.setToken(res, tokens) };
  }

  async logout(req: Request, res: Response) {
    res.clearCookie(TOKEN_TYPES.refreshToken);
    return { message: MESSAGES.AUTH.logoutSuccess };
  }

  async validateUser({ email, password }: AuthDto) {
    const user = await this.usersService.getBy(
      {
        key: USER.email,
        value: email,
      },
      ['user.password'],
    );
    if (!user) throw new BadRequestException(ERRORS.AUTH.wrongCredentials);

    this.checkUser(user);
    await this.verifyPassword(password, user.password);
    delete user.password;

    return user;
  }

  async verifyPassword(password: string, hashedPassword: string) {
    if (hashedPassword === null)
      throw new BadRequestException(ERRORS.AUTH.wrongCredentials);

    const isPasswordMatched = await bcrypt.compare(password, hashedPassword);
    if (!isPasswordMatched)
      throw new BadRequestException([ERRORS.AUTH.wrongCredentials]);
  }

  createTokens({ id, role }): TokensDto {
    const accessToken = this.jwtService.sign(
      {
        id,
        role,
        type: TOKEN_TYPES.accessToken,
      },
      { expiresIn: this.configService.get<string>(JWT_ACCESS_EXP) },
    );
    const refreshToken = this.jwtService.sign(
      {
        id,
        role,
        type: TOKEN_TYPES.refreshToken,
      },
      { expiresIn: this.configService.get<string>(JWT_REFRESH_EXP) },
    );

    return { accessToken, refreshToken };
  }

  async refreshTokens(
    req: Request,
    res: Response,
  ): Promise<{ accessToken: string }> {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken)
      throw new BadRequestException(ERRORS.AUTH.noRefreshToken);

    const decodedJwt = this.jwtService.decode(refreshToken) as IDecodedJwt;
    if (!decodedJwt)
      throw new BadRequestException(ERRORS.AUTH.invalidRefreshToken);

    const { id, type, role } = decodedJwt;

    const user = await this.usersService.getBy({ key: USER.id, value: id });
    if (!user) throw new BadRequestException(ERRORS.USER.notExist);

    if (type !== TOKEN_TYPES.refreshToken)
      throw new BadRequestException(ERRORS.AUTH.noRefreshToken);

    this.jwtService.verify(refreshToken);

    const newTokens = await this.createTokens({ id, role });

    return { accessToken: this.setToken(res, newTokens) };
  }

  setToken(res: Response, tokens: TokensDto) {
    res.cookie(TOKEN_TYPES.refreshToken, tokens.refreshToken, {
      httpOnly: true,
    });
    return tokens.accessToken;
  }

  private checkUser(user: User) {
    if (user.status === EntityStatus.DELETED) {
      throw new BadRequestException(ERRORS.AUTH.accountDeleted);
    }
  }

  async encodePassword(password: string) {
    const encodedPassword = {
      password: (await bcrypt.hash(password, 10)) as string,
    };
    return encodedPassword;
  }
}
