export const JWT_SECRET = 'JWT_SECRET';
export const JWT_ACCESS_EXP = 'JWT_ACCESS_EXP';
export const JWT_REFRESH_EXP = 'JWT_REFRESH_EXP';
export const TOKEN_TYPES = {
  accessToken: 'accessToken',
  refreshToken: 'refreshToken',
};
