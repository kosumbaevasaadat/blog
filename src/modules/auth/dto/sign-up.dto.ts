import { IsNotEmpty, IsString, MinLength } from 'class-validator';

import { AuthDto } from './auth.dto';

export class SignUpDto extends AuthDto {
  @MinLength(1)
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @MinLength(1)
  @IsString()
  @IsNotEmpty()
  lastName: string;
}
