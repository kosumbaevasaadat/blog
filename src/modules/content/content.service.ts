import { Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateContentDto } from './dto/create-content.dto';
import { UpdateContentDto } from './dto/update-content.dto';
import { Content } from '../../entities/content.entity';
import { BaseService } from '../../shared/lib/base.service';
import { FileService } from '../file/file.service';
import { EntityStatus } from '../../shared/enums/entity-status';
import { ERRORS } from '../../shared/constants';

@Injectable()
export class ContentService extends BaseService<Content> {
  constructor(
    @InjectRepository(Content)
    private contentRepository: Repository<Content>,
    private readonly fileService: FileService,
  ) {
    super(contentRepository, 'content');
  }

  async create(
    { description = null }: CreateContentDto,
    file: Express.Multer.File,
  ) {
    const image = await this.fileService.upload(file);
    const content = this.contentRepository.create({ description, image });

    return this.contentRepository.save(content);
  }

  async update(
    id: number,
    updateContentDto: UpdateContentDto,
    file?: Express.Multer.File,
  ) {
    const existContent = await this.getByWithRelations(
      { key: 'id', value: id },
      ['image'],
    );

    const data = { ...updateContentDto };

    if (file) {
      data['image'] = await this.fileService.update(
        file,
        existContent.image.id,
      );
    }

    const content = this.contentRepository.merge(existContent, data);
    await this.contentRepository.save(content);

    return this.getByWithRelations({ key: 'id', value: id }, ['image']);
  }

  async remove(id: number) {
    const existContent = await this.getByWithRelations(
      { key: 'id', value: id },
      ['image'],
    );
    if (!existContent)
      throw new NotFoundException(ERRORS.GENERAL.notFound + id);

    const content = this.contentRepository.merge(existContent, {
      status: EntityStatus.DELETED,
    });

    await this.fileService.removeFileById(existContent.image.id);

    return this.contentRepository.save(content);
  }
}
