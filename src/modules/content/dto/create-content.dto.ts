import { IsOptional, IsString, MaxLength } from 'class-validator';

export class CreateContentDto {
  @IsString()
  @IsOptional()
  @MaxLength(300)
  description: string | null = null;
}
