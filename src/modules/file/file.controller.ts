import {
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Response } from 'express';

import { FileService } from './file.service';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';

const cached = new Map();

@Controller('file')
export class FileController {
  constructor(private fileService: FileService) {}

  @Get(':id')
  async getById(
    @Param('id', ParseIntPipe) id: number,
    @Res({ passthrough: true }) res: Response,
  ) {
    const fileData = await this.fileService.getFileById(id);
    cached.set(fileData.fileKey, { ...fileData, date: +new Date() });
    res.header('Content-Type', cached.get(fileData.fileKey).ContentType);
    res.send(cached.get(fileData.fileKey).Body);
  }

  @Post()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  upload(@UploadedFile() file: Express.Multer.File) {
    return this.fileService.upload(file);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  update(
    @Param('id', ParseIntPipe) id: number,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.fileService.update(file, id);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.fileService.removeFileById(id);
  }
}
