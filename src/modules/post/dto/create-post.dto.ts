import { PartialType } from '@nestjs/mapped-types';
import { CreateContentDto } from '../../content/dto/create-content.dto';

export class CreatePostDto extends PartialType(CreateContentDto) {}
