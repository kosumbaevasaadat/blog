import { Repository } from 'typeorm';
import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { Post } from '../../entities/post.entity';
import { BaseService } from '../../shared/lib/base.service';
import { User } from '../../entities/user.entity';
import { ContentService } from '../content/content.service';
import { IPaginationParams } from '../../shared/types/pagination';
import { EntityStatus } from '../../shared/enums/entity-status';
import { ERRORS, MESSAGES, PAGINATION } from '../../shared/constants';

@Injectable()
export class PostService extends BaseService<Post> {
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
    private readonly contentService: ContentService,
  ) {
    super(postRepository, 'post');
  }

  async create(
    user: User,
    createPostDto: CreatePostDto,
    file: Express.Multer.File,
  ) {
    const content = await this.contentService.create(
      {
        description: createPostDto.description,
      },
      file,
    );

    const post = this.postRepository.create({ content, author: user });
    return this.postRepository.save(post);
  }

  findAll({
    take = PAGINATION.take,
    page = PAGINATION.page,
  }: IPaginationParams) {
    const skip = page === 1 ? 0 : (page - 1) * take;
    return this.repository
      .createQueryBuilder('post')
      .where('post.status = :status', { status: EntityStatus.ACTIVE })
      .leftJoinAndSelect('post.author', 'user')
      .leftJoinAndSelect('post.content', 'content')
      .leftJoinAndSelect('content.image', 'file')
      .take(take)
      .skip(skip)
      .getMany();
  }

  findOne(id: number) {
    return this.repository
      .createQueryBuilder('post')
      .where('post.id = :id', { id })
      .andWhere('post.status = :status', { status: EntityStatus.ACTIVE })
      .leftJoinAndSelect('post.author', 'user')
      .leftJoinAndSelect('post.content', 'content')
      .leftJoinAndSelect('content.image', 'file')
      .getOne();
  }

  async update(
    id: number,
    user: User,
    updatePostDto: UpdatePostDto,
    file?: Express.Multer.File,
  ) {
    const existPost = await this.checkAuthor(id, user);

    return this.contentService.update(
      existPost.content.id,
      updatePostDto,
      file,
    );
  }

  async remove(id: number, user: User) {
    const existPost = await this.checkAuthor(id, user);

    const post = this.postRepository.merge(existPost, {
      status: EntityStatus.DELETED,
    });
    await this.contentService.remove(existPost.content.id);

    await this.postRepository.save(post);

    return { message: MESSAGES.GENERAL.deleted };
  }

  async checkAuthor(id: number, user: User) {
    const existPost = await this.getByWithRelations({ key: 'id', value: id }, [
      'content',
      'author',
    ]);

    if (!existPost) throw new NotFoundException(ERRORS.GENERAL.notFound + id);
    if (existPost.author.id !== user.id)
      throw new ForbiddenException(ERRORS.AUTH.forbidden);

    return existPost;
  }
}
