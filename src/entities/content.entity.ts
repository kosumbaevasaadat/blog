import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { File } from './file.entity';
import { BaseEntity } from './base.entity';

@Entity()
export class Content extends BaseEntity {
  @Column({ nullable: true })
  description: string;

  @OneToOne(() => File, { nullable: false })
  @JoinColumn()
  image: File;
}
