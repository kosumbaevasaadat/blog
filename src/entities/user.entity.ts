import { Column, Entity } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Roles } from '../shared/enums/user-roles';

@Entity()
export class User extends BaseEntity {
  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column({ select: false })
  password: string;

  @Column({ default: Roles.USER })
  role: Roles;
}
