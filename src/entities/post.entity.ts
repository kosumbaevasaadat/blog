import { Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { User } from './user.entity';
import { Content } from './content.entity';

@Entity()
export class Post extends BaseEntity {
  @OneToOne(() => Content, { nullable: false })
  @JoinColumn()
  content: Content;

  @ManyToOne(() => User, { nullable: false })
  author: User;
}
