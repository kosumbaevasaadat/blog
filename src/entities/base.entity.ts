import {
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { EntityStatus } from '../shared/enums/entity-status';

export class BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: EntityStatus.ACTIVE })
  status: EntityStatus;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updateDate: Date;
}
