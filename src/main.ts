import { config } from 'dotenv';
import * as cookieParser from 'cookie-parser';
import * as passport from 'passport';
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';

import { AppModule } from './app.module';

async function bootstrap() {
  config();

  const app = await NestFactory.create(AppModule);
  const PORT = process.env.PORT || 3000;

  app.enableCors({ origin: '*', credentials: true, allowedHeaders: '*' });
  app.setGlobalPrefix('api');
  app.useGlobalPipes(new ValidationPipe());
  app.use(passport.initialize());
  app.use(cookieParser());

  await app.listen(PORT, () =>
    console.log(`Application started on port ${PORT}`),
  );
}

bootstrap();
